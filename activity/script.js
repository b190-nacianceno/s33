// console.log("Hello World");

// const { json } = require("stream/consumers");


// retrieve the to do list
async function fetchData(){
    let result = await fetch ("https://jsonplaceholder.typicode.com/todos");
    // console.log(result);
    // console.log(typeof result);
    // console.log(result.body);
 
    let json = await result.json()
    console.log(json);
    // create an array of the titles using map method
    let array = json.map(json => json.title);
    console.log(array);
};

fetchData();
 


// fetch an item using GET method
fetch("https://jsonplaceholder.typicode.com/todos?id=5", {
    method: "GET", 
    headers: {
        "Content-Type": "application/json"
    },
})
.then(response => response.json())
// .then(response => json.title)
.then(json =>  console.log(`The item "${json[0].title}" on the list has a status of ${json[0].completed}`))

// .then(JSON.parse());
// let object = JSON.parse(toDoFive);


// console.log(`The item ${toDoFive.title} on the list has a status of ${toDOFive.status}`);





// add a new task using POST method
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "New Item",
        body: "Do laundry",
        userId: 2
    })
})
.then(response => response.json())
.then(json => console.log(json));

// Update a post using PUT method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        userId: 1,
        title: "Update this item",
        completed: false
    })
})
.then(response => response.json())
.then(json => console.log(json));

// update data structure
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Update this item",
        description: "Update list with a new structure",
        status: false,
        dateCompleted: "Pending",
        userId: 1,
    })
})
.then(response => response.json())
.then(json => console.log(json));

// update an item using PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        id:5,
        status: "Completed"
    })
})
.then(response => response.json())
.then(json => console.log(json));

// update item to change status to complete and add date completed
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Completed item",
        description: "Updated the date completed",
        status: "Complete",
        dateCompleted: "10-21-22"
    })
})
.then(response => response.json())
.then(json => console.log(json));

// use the delete method
fetch("https://jsonplaceholder.typicode.com/todos/1",{
    method: "DELETE"
    
})



 


