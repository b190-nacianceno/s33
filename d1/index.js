/* 
    SECTION - JS Synchronous vs Asynchronous  
    -JS, by default, is synchronous, meaning that only one statement will be executed at a time.
    =This can be proven when a statement has an error, JavaScript will not proceed with the next statement
*/
/* 
console.log("Hello World");
censole.log("Hello Again")
console.log("Goodbye"); */


// When statements take time to process, this slows down our code.
// An example of this is when loops are used on a large amount of information or when fetching data from databases
// We might not notice it due to  the fast processing power of our devices
//  This the reason some websites don't instantly load and we only see a white screen at times when the app is still waiting for all the code to be executed
// for (let i=1; i <= 1500; i++){
//     console.log(i);

// };
// console.log("It's me again");

// Fetch API - allows us to asynchronously request for a resourse(data);
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

/* 
SYNTAX:
    fetch(URL);
*/

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));


// retrieves all posts following the REST API (retrieve, /posts, GET)
fetch("https://jsonplaceholder.typicode.com/posts")
// by using the .then method, we can now check the status of the promise
// "fetch" method will return a "promise" that resolves to a response object
// .then method captures the "response" object and returns another "promise" which will eventually be resolved or rejected
// .then(response => console.log(response));

// we use the "json" method from teh response object to convert the data retrieved into JSON format 
.then(response => response.json())
// using multiple then methods will result into "promise chain"
// then we can now access it and log in the console our desired output
.then(json => console.log(json));
console.log("Hello");


console.log("Hello");
console.log("Hello World"); 
console.log("Hello Again");


// ASYNC-AWAIT
// "asynch" and "await" keywords are other approcahes that can be used to perfrom asynchronous JS

// used in functions to indicate which portions of the code should be waited for 

// creates an asynchronous function
async function fetchData(){
    // waits for the fetch method to complete and then stores it inside the result var
    let result = await fetch ("https://jsonplaceholder.typicode.com/posts");
    // result of returned fetch
    console.log(result);
    console.log(typeof result);
    console.log(result.body);
 
    let json = await result.json()
    console.log(json);
    

};

fetchData();

console.log("Hello");

// SECTION - creating a post
/* 
SYNTAX:     
    fetch(URL,options)
*/

// create a new post following REST IP(create, /posts, POST)
fetch("https://jsonplaceholder.typicode.com/posts", {
    // setting request method to POST
    method: "POST",
    // setting request headers to be sent to the backend
    // specified that he request headers will be sending JSON structure for its content 
    headers: {
        "Content-Type": "application/json"
    },
    // set content/body data of the "request" object to be sent to the backend
    // JSON.stringify converts the object into stringified JSON
    body: JSON.stringify({
        title: "New Post",
        body: "Hello World",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json));

// Mini-activity
// PUT - update a post
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT", 
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        id: 1,
        title: "Updated Post",
        body: "Hello World",
        userId: 1
    })
})
.then(response => response.json())
.then(json => console.log(json));


// PATCH - update a post

/* 
 the difference between PUT and PATCH is the number of properties being updated PATCH is used to update A SINGLE PROPERTY while maintaining the unupdated properties
 PUT is used when all of the properties need to be updated, or the whole document itself
*/
fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PATCH", 
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        
        title: "Corrected Post",
      
    })
})
.then(response => response.json())
.then(json => console.log(json));

// SECTION - deleting of a resource  
fetch("https://jsonplaceholder.typicode.com/posts/1",{
    method: "DELETE"
    
})

// SECTION -FILTER POSTS 
// the data can be filtered by sending the userId along with the URL
// information sent via the url can be done by adding the question mark symbol(?)

/* 
SYNTAX
    "<url>?parameterName=value" - for single parameter search
    "<url>?parameterName=value&&parameterName=value" - for multiple parameter search
*/
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then(response => response.json())
.then(json => console.log(json));

// Retrieving comments for a specific post/accessing neste/embedded comments

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then(response => response.json())
.then(json => console.log(json));